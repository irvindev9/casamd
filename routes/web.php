<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

/** Medical Certificate PDF **/
Route::GET('/print/medical_certificate','DoctorController@pdf_certificate')->middleware('auth','role:doctor');

/*** Doctor Panel ***/
Route::get('/doctor/panel', function () {
    return view('doctor.panel');
})->name('doctor_home')->middleware('auth','role:doctor');

/*** Doctor SignUp ***/
Route::get('/doctor/signup', function () {
    return view('doctor.signup.form');
});

/*** Doctor Patients List ***/
Route::get('/doctor/patients_list', function () {
    return view('doctor.panel_views.patients_list');
})->middleware('auth','role:doctor');

/*** Doctor Meets ***/
Route::get('/doctor/meet/requests', function () {
    return view('doctor.panel_views.meet_request');
})->middleware('auth','role:doctor');

Route::any('/doctor/signup/save','DoctorController@store')->name('doctor_save');

//Medical Certificate 
Route::get('/doctor/medical_certificate', function () {
    return view('doctor.medical_certificate');
})->middleware('auth','role:doctor');

/*** User Sign Up ***/
Route::get('/user/signup', function () {
    return view('user.signup.form');
});

/*** Patient Sign Up ***/
Route::get('/user/patient/signup', function () {
    return view('user.signup.patient_form');
})->middleware('auth','role:client');

Route::get('/user/patient/signup/editar/{id}', function ($id) {
    //Verificar que sea familiar del usuario
    $paciente = App\patient::where('id',$id)->where('user_id',auth()->user()->id)->first();

    if(!isset($paciente)){
        return redirect()->route('user_home');
    }
    return view('user.signup.patient_form',['id' => $id]);
})->middleware('auth','role:client');

//User Sign In
Route::get('/user/login', function () {
    return view('login');
})->name('inicio_sesion');

//User general panel
Route::get('/user/control_panel', function () {
    return view('user.panel');
})->middleware('auth','role:client');

//Route to save the info
Route::post('/user/signup/save', 'users@signup');

//Route to update meet status
Route::get('/meet/status/update', 'DoctorController@meet_status_update');

//para evitar problemas
Route::get('/user/signup/save', 'users@signup');

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/login','Auth\LoginController@login')->name('login');
Route::get('/login','Auth\LoginController@login')->name('login');

Route::get('/logout','Auth\LoginController@logout')->name('logout');
Route::post('/logout','Auth\LoginController@logout')->name('logout');


//Perfil del usuario
Route::get('/user/home','user_home@home')->name('user_home');
//Panel de citas
Route::get('/user/solicitar','user_home@solicitar')->name('user_solicitar');
//Salvar citas
Route::any('/user/solicitar/save','AppointmentController@store')->name('solicitar_save');


//Configuracion de membresia
Route::get('/user/home/configuracion','user_home@configuracion')->name('user_configuracion');

//Info cita
Route::get('/user/home/cita/info/{id}','user_home@cita')->name('info_cita');

//Recargar
Route::get('/user/home/recargar/{id}','user_home@recargar')->name('recargar');

//Guardar informacion de pacientes
Route::post('/user/patient/signup/save','PatientController@store')->name('guardar_paciente');
//Para evitar problemas
Route::get('/user/patient/signup/save','PatientController@store')->name('guardar_paciente');
//Para pago
Route::get('/user/home/configuracion/pago','user_home@pago')->name('user_pago');