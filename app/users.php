<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users extends Model
{
    protected $table = 'user_general_info';													

    protected $fillable = [
        'user_id',
        'name',
        'last_name',
        'email',
        'cellphone_phone',
        'phone_number',
        'neighborhood',
        'street',
        'ext_number',
        'int_number',
        'created_at',
        'updated_at'
    ];
}
