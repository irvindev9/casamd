<?php

namespace App\Http\Controllers;


use App\doctor;
use App\appoinment;
use App\login;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use PDF;
use Illuminate\Support\Facades\DB;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->hasFile('file')){
            return 1;
        }
        //Guardar informacion de login
        $login = new login();
        $login->username = $request->nombre_completo;
    	$login->user_type = 'doctor';
    	$login->user_email = $request->correo_electronico;
        $login->password = bcrypt($request->password);
        $login->remember_token = Str::random(10);
        $login->active = '1';
        $login->save();

        // guardar info de doctor
        $doctor = new doctor();
        $doctor->user_id = $login->id;
        $doctor->name = $request->nombre_completo;
        $doctor->college = $request->universidad_egreso;
        $doctor->birthdate = $request->edad;
        $doctor->email = $request->correo_electronico;
        $doctor->cellphone_number = $request->telefono_adicional;
        $doctor->phone_number = $request->telefono_principal;
        //Guardar foto
        if($request->hasFile('file')){
            $extension = $request->file('file')->getClientOriginalExtension();
            $dir = 'img/perfiles_fotos/';
            $filename = $doctor->name.'-'.time() . '.' . $extension;
            $request->file('file')->move($dir, $filename);
            // Set user profile image path in database to filePath
            $doctor->photo = $filename;
        }
        $doctor->neighborhood = $request->colonia_consultorio;
        $doctor->street = $request->calle_consultorio;
        $doctor->ext_number = $request->num_exterior_consultorio;
        $doctor->int_number = $request->num_interior_consultorio;
        $doctor->zipcode = $request->codigo_postal_consultorio;
        $doctor->other = $request->detalles_adicionales_consultorio;
        $doctor->save();


        $credenciales = array(
            'user_email' => $request->correo_electronico,
            'password' => $request->password,
            'active' => 1
        );
    
          //return $credenciales;
    
        if(Auth::attempt($credenciales)){
            return redirect()->route('doctor_home');
        }

        return redirect()->route('doctor_home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function show(doctor $doctor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function edit(doctor $doctor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, doctor $doctor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function destroy(doctor $doctor)
    {
        //
    }

    public function pdf_certificate(){

        $data = [
            'nombre_paciente' => 'Nombre De Paciente Completo',
            'sexo_paciente' => 'Masculino',
            'edad_paciente' => '18',
            'tension_arterial' => '120/80',
            'frecuencia_cardiaca' => '16',
            'frecuencia_respiratoria' => '16',
            'temperatura' => '33',
            'peso' => '45',
            'talla' => '34',
            'estado_salud' => 'Saludable',
            'ciudad' => 'Juarez',
            'dias' => '15',
            'mes' => 'Julio',
            'year' => '2019',
            'doctor' => 'Alonso Garcia Del Rio',
            'cedula_profesional' => '15258564',
            'universidad_egreso' => 'Universidad Autonoma De Cd. Juarez',
        ];

        $pdf = PDF::loadView('doctor.pdfs.medical_certificate_pdf');
  
        return $pdf->stream('Certificado.pdf');
    }

    public function meet_status_update(Request $request){
        DB::table('appointment_details')
        ->where('id', $request->solicitud_id)
        ->update(['is_acepted' => 1]);

        return redirect('/doctor/panel');
    }
}
