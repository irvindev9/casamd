<?php

namespace App\Http\Controllers;

use App\active_tier;
use Illuminate\Http\Request;

class ActiveTierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\active_tier  $active_tier
     * @return \Illuminate\Http\Response
     */
    public function show(active_tier $active_tier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\active_tier  $active_tier
     * @return \Illuminate\Http\Response
     */
    public function edit(active_tier $active_tier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\active_tier  $active_tier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, active_tier $active_tier)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\active_tier  $active_tier
     * @return \Illuminate\Http\Response
     */
    public function destroy(active_tier $active_tier)
    {
        //
    }
}
