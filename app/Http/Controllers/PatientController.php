<?php

namespace App\Http\Controllers;

use App\patient;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->edicion)){
            //Edicion
            $paciente = patient::find($request->edicion);
            $paciente->user_id = auth()->user()->id;
            $paciente->name = $request->nombre_completo;
            $paciente->gender = $request->sexo;
            $paciente->age = $request->edad;
            $paciente->other = $request->consideraciones;
            $paciente->cellphone_phone = $request->numero_celular;
            $paciente->phone_number = $request->numero_telefono;
            $paciente->neighborhood = $request->colonia;
            $paciente->street = $request->calle;
            $paciente->ext_number = $request->numero_exterior;
            $paciente->int_number = $request->numero_exterior;
            $paciente->save();
        }else{
            //Guarda la info del paciente
            $paciente = new patient();
            $paciente->user_id = auth()->user()->id;
            $paciente->name = $request->nombre_completo;
            $paciente->gender = $request->sexo;
            $paciente->age = $request->edad;
            $paciente->other = $request->consideraciones;
            $paciente->cellphone_phone = $request->numero_celular;
            $paciente->phone_number = $request->numero_telefono;
            $paciente->neighborhood = $request->colonia;
            $paciente->street = $request->calle;
            $paciente->ext_number = $request->numero_exterior;
            $paciente->int_number = $request->numero_exterior;
            $paciente->save();
        }
        

        return redirect()->route('user_home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function show(patient $patient)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function edit(patient $patient)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, patient $patient)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function destroy(patient $patient)
    {
        //
    }
}
