<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
  public function login(Request $request){
    $this->validate(request(),[
        'user_email' => 'email|required|string',
        'user_password' => 'required|string'
    ]);

    $credenciales = array(
        'user_email' => $request->user_email,
        'password' => $request->user_password,
        'active' => 1
    );

      //return $credenciales;

      if(Auth::attempt($credenciales)){
        return redirect()->route('user_home');
      }else{
        return back()->withErrors(['email' => trans('auth.failed')])->withInput([$request->user_email]);
      }
  }

  public function logout(){
    Auth::logout();
    return redirect('/');
  }
  
}
