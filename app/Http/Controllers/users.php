<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\users as clients;
use App\login;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class users extends Controller
{   
    //Funcion para guardar los registros
    public function signup(Request $request){
        //Se verifica que existan datos en el request
        if(!isset($request)){
            //Si no hay datos redirige al inicio
            return redirect('/');
        }
        
        //Se guardan los datos de login / inicio de sesion
        $login = new login();
        $login->username = $request->complete_name;
    	$login->user_type = 'client';
    	$login->user_email = $request->email;
        $login->password = bcrypt($request->password);
        $login->remember_token = Str::random(10);
        $login->active = '1';
        $login->save();

        //Se procede a guardar los datos del usuario
        $save = new clients();
        $save->user_id = $login->id;
        $save->name = $request->complete_name;
        //$save->last_name = $request->aaaaaa;
        $save->email = $request->email;
        $save->cellphone_phone = $request->cellphone_number;
        $save->phone_number = $request->phone_number;
        $save->neighborhood = $request->neighborhood;
        $save->street = $request->street;
        $save->ext_number = $request->external_number;
        $save->int_number = $request->internal_number;
        $save->save();

        $credenciales = array(
            'user_email' => $request->email,
            'password' => $request->password,
            'active' => 1
        );
    
          //return $credenciales;
    
        if(Auth::attempt($credenciales)){
            return redirect()->route('user_home');
        }

        return redirect()->route('user_home');
    }
}
