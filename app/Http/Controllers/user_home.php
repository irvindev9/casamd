<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class user_home extends Controller
{
    //Check auth
    public function __construct()
    {
        $this->middleware('auth');
    }


    //Vistas
    public function home(){
        return view('user.home.panel');
    }

    public function configuracion(){
        return view('user.home.configuracion');
    }

    public function cita($id_cita){
        return view('user.home.cita_info',['id_cita' => $id_cita]);
    }

    public function recargar($id = 0){
        return view('user.home.recargar',['id_servicio' => $id]);
    }

    public function solicitar(){
        return view('user.home.solicitar');
    }

    public function pago(){
        return view('user.home.pago');
    }
}
