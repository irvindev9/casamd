<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class login extends Model
{
    protected $table = 'users';													

    protected $fillable = [
        'id',
        'username',
    	'user_type',
    	'user_email',
        'password',
        'remember_token',
        'active',
    	'created_at',
    	'updated_at',
    ];
}
