@extends('index')

@section('container')
    <?php
        $hoy = date("Y-m-d");   
        $subscripcion = App\active_tier::where('id_user',auth()->user()->id)->where('expire_date','>',$hoy)->first();
        if(isset($subscripcion)){
            $sub = App\tier::find($subscripcion->id_tier)->tier_name;
            $date_exp = $subscripcion->expire_date;
        }else{
            $sub = "No hay subscripcion activa";
            $date_exp = '--/--/----';
        }
    ?>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-6">
                <a class="btn btn-block btn-sm btn-light bold" href="#">Mi panel de control</a>
            </div>
            <div class="col-6">
                <a class="btn btn-block btn-sm btn-light bold" href="/user/solicitar">Servicios</a>
            </div>
        </div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action">
                      <div class="d-flex w-100 justify-content-between">
                        <label style="color:black; height:2.5em; color:white; background-color:#33C2B7; font-size:1.3em; width:20em; margin-left:-0.7em; padding-top:0.5em; text-align:center; border-radius:15px 0px 15px 0px;"><b>Informacion de tu suscripcion</b></label>
                        <h5 class="mb-1"></h5>
                        <small><button onclick="window.location.href='/user/home/configuracion'" class="custom_btn btn btn-secondary btn-sm">Configurar</button></small>
                      </div>
                      <p class="mb-1">Tu suscripcion: {{$sub}}</p>
                      <small>Proximo pago: {{$date_exp}}</small>
                    </a>
                    <a href="#" class="list-group-item list-group-item-action">
                        <h5>Servicios disponibles</h5>
                        <style>
                            .tabla_servicios{
                                max-width: 100%;
                                overflow-x: scroll;
                            }
                        </style>
                        <div class="tabla_servicios">
                        <table class="table">
                            <tr>
                                @foreach(App\service::get() as $servicio)
                                <td nowrap>
                                    <center>
                                    <small>{{$servicio->nombre}}:</small> 1
                                    <br>
                                    <button onclick="window.location.href='/user/home/recargar/{{$servicio->id}}'" class="btn custom_btn btn-success btn-sm">Recargar</button>
                                    </center>
                                </td>
                                @endforeach
                            </tr>
                        </table>
                        </div>
                    </a>
                    <a href="#" class="list-group-item list-group-item-action">
                      <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Bienvenido: {{auth()->user()->username}}</h5>
                        <small class="text-muted"><button onclick="window.location.href='/user/patient/signup'" class="btn custom_btn btn-secondary btn-sm">Agregar</button></small>
                      </div>
                      <p class="mb-1">Tus familiares:</p>
                        <div class="row">
                            @foreach($pacientes = App\patient::where('user_id',auth()->user()->id)->get() as $paciente)
                            <div class="col-sm-4">
                                <div class="card border-secondary mb-3" style="max-width: 18rem;">
                                        <div class="card-header" style="background-color:#226383;"><button onclick="window.location.href='/user/patient/signup/editar/{{$paciente->id}}'" class="btn square_btn btn-block btn-secondary">Editar</button></div>
                                        <div class="card-body text-secondary">
                                            <b class="card-title">{{$paciente->name}}</b>
                                            <p class="card-text">
                                                <b>Edad</b>: {{$paciente->age}} <br>
                                                <b>Sexo</b>: {{$paciente->gender}} <br>
                                                <b>Otro</b>: {{$paciente->other}}    
                                            </p>
                                        </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        
                    </a>
                    <a href="#" class="list-group-item list-group-item-action">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">Citas programadas</h5>
                        </div>
                        @foreach($citas = App\appointment::where('user_id',auth()->user()->id)->get() as $cita)
                        <p class="mb-1">
                            <button  onclick="window.location.href='/user/home/cita/info/{{$cita->id}}'" class="btn custom_btn btn-primary btn-sm">Info</button> Doctor: {{App\doctor::where('user_id',$cita->doctor_id)->first()->name}} / Paciente: {{App\patient::find($cita->user_patient_id)->name}} / {{$cita->type}} / {{$cita->appointment_date}} / {{$cita->appointment_time}}
                        </p>
                        @endforeach
                    </a>
                </div>
            </div>
        </div>
    </div>

@stop