@extends('index')

@section('container')
    <?php
        $hoy = date("Y-m-d");   
        $subscripcion = App\active_tier::where('id_user',auth()->user()->id)->where('expire_date','>',$hoy)->first();
        if(isset($subscripcion)){
            $sub = App\tier::find($subscripcion->id_tier)->tier_name;
            $date_exp = $subscripcion->expire_date;
        }else{
            $sub = "No hay subscripcion activa";
            $date_exp = '--/--/----';
        }
    ?>
    <div class="container">
        <div class="row alert alert-info">
            <div class="col-6">
                Tu suscripcion actual: {{$sub}}
            </div>
            <div class="col-6">
                Activo hasta: {{$date_exp}} <button class="square_btn btn btn-sm btn-primary">Cancelar suscripcion</button>
            </div>
        </div>
    </div>
    <br><br>
    <style>
        .titulo_tier{
            font-weight: bold;
            color: #00675b!important;
            font-size: 35px;
        }

        .titulo_tier_precio{
            font-weight: bold;
            color: #00675b!important;
            font-size: 30px;
        }

        .tier_titulo_fondo{
            background-color: #d2f7f3;
            opacity: 0.9;
        }

        .list-productos{
            text-align: left;
            font-size: 12px;
        }

        .btn-doc{
            background-color:#33C2B7; 
            border:solid 1px #33C2B7;
        }
        
    </style>
    <div class="container">
        <div class="row">
            <div class="col-10 offset-1">
                <div class="row">
                    <div class="col-4">
                        <div class="container">
                            <div class="card text-center col-12 tier_titulo_fondo">
                                <div class="card-header tier_titulo_fondo ">
                                    <a class="titulo_tier">{{App\tier::find(1)->tier_name}}</a>
                                    <br>
                                    <small>{{App\tier::find(1)->description}}</small>
                                </div>
                                <div class="card-body">
                                    <small>$</small><a class="titulo_tier_precio">{{App\tier::find(1)->tier_value}}</a><small>MXN/Mes</small>
                                    <br>
                                    <small class="card-text">Pago automatico cada mes*</small>
                                    <br>
                                    <a href="#" class="btn btn-primary btn-block custom_btn">Pagar ahora</a>
                                </div>
                                <div class="card-footer text-muted">
                                    <p class="list-productos">
                                        <img src="https://img.icons8.com/material-outlined/24/000000/check-all.png"> 5 consultas al mes
                                    </p>
                                    <p class="list-productos">
                                        <img src="https://img.icons8.com/material-outlined/24/000000/check-all.png"> 5 teleconsultas al mes
                                    </p>
                                    <p class="list-productos">
                                        <img src="https://img.icons8.com/material-outlined/24/000000/check-all.png"> 5 servicios adicionales al mes
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="container">
                            <div class="card text-center col-12 tier_titulo_fondo">
                                <div class="card-header tier_titulo_fondo ">
                                    <a class="titulo_tier">{{App\tier::find(2)->tier_name}}</a>
                                    <br>
                                    <small>{{App\tier::find(2)->description}}</small>
                                </div>
                                <div class="card-body">
                                    <small>$</small><a class="titulo_tier_precio">{{App\tier::find(2)->tier_value}}</a><small>MXN/Mes</small>
                                    <br>
                                    <small class="card-text">Pago automatico cada mes*</small>
                                    <br>
                                    <a href="#" class="btn btn-primary btn-block custom_btn">Pagar ahora</a>
                                </div>
                                <div class="card-footer text-muted">
                                    <p class="list-productos">
                                        <img src="https://img.icons8.com/material-outlined/24/000000/check-all.png"> 10 consultas al mes
                                    </p>
                                    <p class="list-productos">
                                        <img src="https://img.icons8.com/material-outlined/24/000000/check-all.png"> 10 teleconsultas al mes
                                    </p>
                                    <p class="list-productos">
                                        <img src="https://img.icons8.com/material-outlined/24/000000/check-all.png"> 10 servicios adicionales al mes
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="container">
                            <div class="card text-center col-12 tier_titulo_fondo">
                                <div class="card-header tier_titulo_fondo ">
                                    <a class="titulo_tier">{{App\tier::find(3)->tier_name}}</a>
                                    <br>
                                    <small>{{App\tier::find(3)->description}}</small>
                                </div>
                                <div class="card-body">
                                    <small>$</small><a class="titulo_tier_precio">{{App\tier::find(3)->tier_value}}</a><small>MXN/Mes</small>
                                    <br>
                                    <small class="card-text">Pago automatico cada mes*</small>
                                    <br>
                                    <a href="#" class="btn btn-primary btn-block custom_btn">Pagar ahora</a>
                                </div>
                                <div class="card-footer text-muted">
                                    <p class="list-productos">
                                        <img src="https://img.icons8.com/material-outlined/24/000000/check-all.png"> 15 consultas al mes
                                    </p>
                                    <p class="list-productos">
                                        <img src="https://img.icons8.com/material-outlined/24/000000/check-all.png"> 15 teleconsultas al mes
                                    </p>
                                    <p class="list-productos">
                                        <img src="https://img.icons8.com/material-outlined/24/000000/check-all.png"> 15 servicios adicionales al mes
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop