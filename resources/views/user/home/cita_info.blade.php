@extends('index')

@section('container')
    <style>
        .title_cita_info{
            font-weight: bold;
            font-size: 20px;
        }

        .btn-doc{
            background-color:#33C2B7; 
            border:solid 1px #33C2B7;
        }
    </style>
    <?php
        $info = App\appointment::find($id_cita);
    ?>
    <div class="container">
        <div class="row">
            <div class="card col-md-3 offset-md-1">
                <p class="title_cita_info">
                    <img src="https://img.icons8.com/color/48/000000/doctor-male.png"> Doctor
                </p>
                <p>
                    <b>Nombre:</b> {{App\doctor::where('user_id',$info->doctor_id)->first()->name}}
                </p>
                <p>
                    <b>Telefono:</b> {{App\doctor::where('user_id',$info->doctor_id)->first()->phone_number}}
                </p>
                <p>
                    <b>Horario:</b> {{App\doctor::where('user_id',$info->doctor_id)->first()->other}}
                </p>
                <p>
                    <b>Consultorio:</b> {{App\doctor::where('user_id',$info->doctor_id)->first()->street}} {{App\doctor::where('user_id',$info->doctor_id)->first()->ext_number}} {{App\doctor::where('user_id',$info->doctor_id)->first()->neighborhood}}
                </p>
            </div>
            <div class="card col-md-3 offset-md-2">
                <p class="title_cita_info">
                    <img src="https://img.icons8.com/color/48/000000/pay-date.png"> Cita
                </p>
                <p>
                    <b>Paciente:</b> {{App\patient::find($info->user_patient_id)->name}}
                </p>
                <p>
                    <b>Servicio:</b> {{App\service::find($info->type)->nombre}}
                </p>
                <p>
                    <b>Lugar cita:</b> {{$info->place}}
                </p>
                <p>
                    <b>Fecha:</b> {{$info->appointment_date}}
                </p>
                <p>
                    <b>Hora:</b> {{$info->appointment_time}}
                </p>
            </div>
            <div class="col-12 col-md-3 offset-md-1">
                <br>
                <a href="/user/home" class="btn btn-info btn-block btn-sm btn-doc"><img src="https://img.icons8.com/metro/18/FFFFFF/undo.png"> Regresar</a>
            </div>
        </div>
    </div>

@stop