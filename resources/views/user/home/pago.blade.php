@extends('index')

@section('container')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-8 shadow p-3 mb-5 bg-white rounded">
            <div class="row rounded align-items-center class1">
                <div class="col">
                    DETALLES
                    <br>
                    DE PAGO
                </div>
                <div class="col">  
                    <img class="float-right" src="{{ asset('img/VISA.png') }}" style="width:40%;height:50%;">
                </div> 
            </div>
            
            <form action="" class="sumitform" >

                <div class="row class2">
                    <div class="col">
                        <label for="nombre">Nombre y apellido</label>
                        <input type="text" class="form-control" placeholder="" name="nombre">
                    </div>
                    <div class="col">
                        <label for="numerotarjeta">Número de tarjeta</label>
                        <input class="form-control" id="card_number" name="numerotarjeta" pattern="[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}" required>
                    </div>
                </div>

                <div class="row class2"> 
                    <div class="col">
                        <label for="">Fecha de expiración</label>
                        <select class="form-control" id="ok">
                            <option value="01">Enero</option>
                            <option value="02">Febrero</option>
                            <option value="03">Marzo</option>
                            <option value="04">Abril</option>
                            <option value="05">Mayo</option>
                            <option value="06">Junio</option>
                            <option value="07">Julio</option>
                            <option value="08">Agosto</option>
                            <option value="09">Septiembre</option>
                            <option value="10">Octubre</option>
                            <option value="11">Noviembre</option>
                            <option value="12">Diciembre</option>
                        </select>
                    </div>

                    <div class="col">
                        <label for="" style="visibility:hidden">Fecha de Expiración</label>
                        <select class="form-control" id="ok">
                            <?php
                                $year = date('Y');
                            ?>
                            @for($i = 0;$i <= 20;$i++)
                                <option>{{($year+$i)}}</option>
                            @endfor
                        </select>
                    </div>

                    <div class="col">
                        <label for="">CVV</label>
                        <input class="form-control" id="cvv" name="cvv" pattern="[0-9]{3}" required>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-8 ">
            <button type"button" class="submit btn btn-primary float-right">Enviar</button>
        </div>
    </div>
</div>

@stop