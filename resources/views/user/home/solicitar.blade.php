@extends('index')

@section('container')

<style>
    .btn-doc{
        background-color:#33C2B7; 
        border:solid 1px #33C2B7;
    }

    .bold{
        font-weight: bold;
        color: #495057;
    }

    .box{
        border: 1px solid rgba(0, 0, 0, 0.125);
        background-color:#fff!important;
        padding: 0.75rem 1.25rem;
        width: 50%!important;
        margin-left:0%;
    }
</style>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-6">
                <a class="btn btn-block btn-sm btn-light bold" href="/user/home">Mi panel de control</a>
            </div>
            <div class="col-6">
                <a class="btn btn-block btn-sm btn-light bold" href="#">Servicios</a>
            </div>
        </div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-12" style="background-color:white; border-radius:15px;">
                <label class="section_name"><b>Solicitud De Servicio</b></label>

                <br>

                <label style="color:grey; font-size:0.9em; margin-top:1em;">A continuación, llene todos los espacios para solicitar un servicio.<br><b style="color:#226383;">NOTA: </b>Los campos marcados con <span style="color:red;">*</span> son obligatorios.</label>

                <form id="service_form" class="row" method="POST" action="{{route('solicitar_save')}}">
                    @csrf
                    <div class="container">
                        <div class="row">
                            <div class="form-group col-12 col-lg-4">
                                <label for="select_service_type" class="input_label">Tipo de servicio <span style="color:red;">*</span></label>
                                <select class="custom_input form-control" id="select_service_type" name="servicio">
                                    <option value="default" class="custom_default">Seleccione el servicio que requiere</option>
                                    @foreach(App\service::get() as $servicio)
                                        <option value="{{$servicio->id}}">{{$servicio->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-12 col-lg-4">
                                <label for="select_paciente" class="input_label">Persona que requiere el servicio (Personal/Familiar) <span style="color:red;">*</span></label>
                                <select class="custom_input form-control" id="select_paciente" name="paciente">
                                    <option value="default" class="custom_default">Seleccione la persona que requiere el servicio</option>
                                    @foreach(App\patient::where('user_id',auth()->user()->id)->get() as $paciente)
                                        <option value="{{$paciente->id}}">{{$paciente->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-12 col-lg-4">
                                <label class="input_label" for="select_doctor">Doctores Disponibles <span style="color:red;">*</span></label>
                                <select class="custom_input form-control" id="select_doctor" name="doctor">
                                    <option value="default">Seleccione un doctor de la lista</option>
                                    @foreach(App\doctor::get() as $doc)
                                        <option value="{{$doc->user_id}}">{{$doc->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row d-flex justify-content-center">
                            <div class="form-group col-12 col-sm-6 col-lg-3">
                                <label class="input_label" for="exampleFormControlSelect1">Fecha <span style="color:red;">*</span></label>
                                <input id="fecha_cita" type="date" class="custom_input form-control" name="fecha">
                            </div>
                            <div class="form-group col-12 col-sm-6 col-lg-3">
                                <label class="input_label" for="exampleFormControlSelect1">Hora <span style="color:red;">*</span></label>
                                <input id="hora_cita" type="time" class="custom_input form-control" name="hora">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-12 col-lg-6">
                                <label for="select_service_type" class="input_label">Seleccione esta opción si desea que el Dr. Asista a su domicilio</label>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1" name="domicilio">
                                    <label style="font-size:0.8em; padding-top:0.2em; color:grey;" class="custom-control-label" for="customCheck1">Servicio a domicilio</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-12 col-sm-8 col-md-6 col-lg-6">
                                <label class="input_label" for="exampleFormControlTextarea1">Información Adicional</label>
                                <textarea class="custom_input form-control" id="exampleFormControlTextarea1" rows="3" name="adicional"></textarea>
                            </div>
                        </div>

                        <div class="row d-flex justify-content-center">
                            <div class="form-group col-12 col-sm-6 col-md-4">
                                <button id="btn_service_request" type="button" class="custom_btn btn btn-block btn-info">Solicitar Servicio</button>
                            </div>
                        </div>
                    </div>
                  </form>
            </div>
        </div>
    </div>



    <!-- Button Modal Activate General Consent -->
    <button type="button" id="btn_modal_consentimiento_general" class="btn d-none btn-primary" data-toggle="modal" data-target="#carta_consentimiento_general">
            Launch demo modal
    </button>
    
    <!-- Modal General consent -->
    <div class="modal fade" id="carta_consentimiento_general" tabindex="-1" role="dialog" aria-labelledby="carta_consentimiento_generalTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="border-radius:15px;">
            <div class="modal-header">
                <label style="color:black; height:2.5em; color:white; background-color:#33C2B7; font-size:1.3em; margin-left:-0.8em; margin-top:-0.8em; padding-top:0.5em; padding-left:1em; padding-right:1em; text-align:center; border-radius:15px 0px 15px 0px;"><b>Carta de Consentimiento</b></label>
            </div>
            <div class="modal-body">
                <p style="color:grey; font-size:0.9em;">
                    Por medio de este conducto y en pleno uso de mis facultades mentales solicito
                    voluntariamente los servicios profesionales de fundación Best A.C y del
                    Dr.(a) <span style="color:#33C2B7;">ALONSO GARCIA DEL RIO</span> quien es médico y cuenta con título y
                    cédula profesional a la vista, a fin de resolver los problemas de salud que me aquejan y que
                    son de su competencia.
                    
                </p>
                <p style="color:grey; font-size:0.9em;">
                    Hago constar que he recibido del médico información clara y completa respecto a (los)
                    problema(s) que comprometen mi salud actualmente y sus riesgos. se me ha explicado con
                    detalle el plan que el medico ha trazado para afinar el diagnóstico y llevar a cabo el tratamiento.

                </p>
                <p style="color:grey; font-size:0.9em;">
                    Después de considerar toda esta información autorizo al Dr.(a) <span style="color:#33C2B7;">ALONSO GARCIA DEL RIO</span> para que solicite los estudios y de más procedimientos de
                    apoyo que juzgue necesario para afinar el diagnóstico y para que instruya en mí el
                    tratamiento que a su juicio sea el adecuado.
                    
                </p>
                <p style="color:grey; font-size:0.9em;">
                    Declaro que la información contenida en este expediente es la que yo le he proporcionado a
                    mi médico que es completa y veraz y que no he omitido datos respecto a mi estado de salud
                    previo ni actual.
                </p>
                <p style="color:grey; font-size:0.9em;">
                    De este modo relevo al Dr.(a) <span style="color:#33C2B7;">ALONSO GARCIA DEL RIO</span> de cualquier
                    responsabilidad presente o futura si ocurriesen complicaciones derivadas de información
                    insuficiente sobre dichos aspectos.
                </p>
            </div>
            <div class="modal-footer">
                <div class="container">
                    <div class="row d-flex justify-content-center">
                        <div class="col-12 col-lg-4" style="margin-top:0.5em;">
                            <button type="button" class="btn btn-block btn-danger" data-dismiss="modal">Cancelar</button>
                        </div>
                        <div class="col-12 col-lg-4" style="margin-top:0.5em;">
                            <button type="button" id="acepta_consentimiento" class="btn btn-block btn-info" data-dismiss="modal" style="background-color:#01A299; border:solid 1px #01A299;" data-toggle="modal" data-target="#aviso_modal">Aceptar y solicitar servicio</button>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>

    <!-- Button Modal Activate Injection Consent -->
    <button type="button" id="btn_injection_modal" class="btn d-none btn-primary" data-toggle="modal" data-target="#carta_consentimiento_inyeccion">
            Launch demo modal
    </button>

    <!-- Modal Injection Consent -->
    <div class="modal fade" id="carta_consentimiento_inyeccion" tabindex="-1" role="dialog" aria-labelledby="carta_consentimiento_inyeccionTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="border-radius:15px;">
            <div class="modal-header">
                <label style="color:black; height:2.5em; color:white; background-color:#33C2B7; font-size:1.3em; margin-left:-0.8em; margin-top:-0.8em; padding-top:0.5em; padding-left:1em; padding-right:1em; text-align:center; border-radius:15px 0px 15px 0px;"><b>Carta de Consentimiento de Inyección</b></label>
            </div>
            <div class="modal-body">
                <p style="color:grey; font-size:0.9em; text-align:justify;">
                    Por medio de este conducto y en pleno uso de mis facultades mentales solicito voluntariamente los servicios profesionales de fundación Best A.C y del Dr.(a) <span style="color:#33c2b7;">ALONSO GARCIA DEL RIO</span>, quien es médico y cuenta con título y cedula
                    profesional a la vista.

                </p>
                <p style="color:grey; font-size:0.9em; text-align:justify;">
                    Hago constar que he recibido del médico información clara y completa en un lenguaje sencillo y comprensible a mi
                    persona respecto a (los) problema(s) que comprometen mi salud actualmente y sus riesgos que conlleva este
                    procedimiento que solicito de manera voluntaria y sin presión alguna.
                    me niego a recibir una valoración medica por el medico que hago mención de fin de que el profesional de la salud pueda
                    establecer un diagnóstico y trazar un tratamiento adecuado para mi persona.
                    después de considerar toda esta información y de aclarar todas mis dudas que he planteado autorizo al médico para
                    realizar en mi persona el procedimiento de <span style="color:#33C2B7;">ALONSO GARCIA DEL RIO</span>.
                </p>
                <p style="color:grey; font-size:0.9em; text-align:justify;">
                    Declaro que la información contenida en este expediente es la que yo le he proporcionado a mi médico que es completa
                    y veraz y que no he omitido datos respecto a mi estado de salud previo ni actual. de este modo relevo al médico de
                    cualquier responsabilidad presente o futura si ocurriesen complicaciones derivadas de información insuficiente sobre
                    dichos aspectos.
                </p>
            </div>
            <div class="modal-footer">
                <div class="container">
                    <div class="row d-flex justify-content-center">
                        <div class="col-12 col-lg-4" style="margin-top:0.5em;">
                            <button type="button" class="btn btn-block btn-danger" data-dismiss="modal">Cancelar</button>
                        </div>
                        <div class="col-12 col-lg-4" style="margin-top:0.5em;">
                            <button type="button" id="acepta_consentimiento_inyeccion" class="btn btn-block btn-info" data-dismiss="modal" style="background-color:#01A299; border:solid 1px #01A299;" data-toggle="modal" data-target="#aviso_modal">Aceptar y solicitar servicio</button>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
  
  <!-- Modal -->
  <div class="modal fade" id="aviso_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Nota</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <p style="color:grey; font-size:0.9em; text-align:justify;">
                El doctor se comunicará con usted con la información que suministro al sistema,
                le pedimos este al pendiente.
            </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-block btn-danger" data-dismiss="modal">Cancelar</button>
          <button type="button" id ="guardar" class="btn btn-block btn-info" data-dismiss="modal" style="background-color:#01A299; border:solid 1px #01A299;">Guardar</button>
        </div>
      </div>
    </div>
  </div>
    
<script>

    //Function launch modals concent
    $('#btn_service_request').on('click',function(){
        var flag = 0;

        if($('#select_service_type').val() != 'default'){
            flag++;
            $('#select_service_type').removeClass('is-invalid');
        }else{
            flag = 0;
            $('#select_service_type').addClass('is-invalid');
        }

        if($('#select_paciente').val() != 'default'){
            flag++;
            $('#select_paciente').removeClass('is-invalid');
        }else{
            flag = 0;
            $('#select_paciente').addClass('is-invalid');
        }

        if($('#select_doctor').val() != 'default'){
            flag++;
            $('#select_doctor').removeClass('is-invalid');
        }else{
            flag = 0;
            $('#select_doctor').addClass('is-invalid');
        }

        if($('#fecha_cita').val().trim() != ''){
            flag++;
            $('#fecha_cita').removeClass('is-invalid');
        }else{
            flag = 0;
            $('#fecha_cita').addClass('is-invalid');
        }

        if($('#hora_cita').val().trim() != ''){
            flag++;
            $('#hora_cita').removeClass('is-invalid');
        }else{
            flag = 0;
            $('#hora_cita').addClass('is-invalid');
        }

        //flag goal = 5
        if(flag == 5){
            if($('#select_service_type').val() == 'Inyeccion'){
                $('#btn_injection_modal').click();
            }else{
                $('#btn_modal_consentimiento_general').click();
            }
        }
        
        
    })
    //Function send form
    $('#guardar').on('click',function(){   
        $('#service_form').submit();    
    })

    //Function Modal General Consent
    /*$('#acepta_consentimiento').on('click',function(){   
        $('#service_form').submit();    
    })

    //Function Modal Injection Consent
    $('#acepta_consentimiento_inyeccion').on('click',function(){
        $('#service_form').submit();
    })*/
</script>

@stop