@extends('index')

@section('container')

    <div class="container">
        <div class="row">
            <div class="col-10 offset-1 card table-responsive">
                <center>
                    <table class="table" width="100%">
                        <tr>
                            <th>Servicio</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th>Subtotal</th>
                        </tr>
                        @foreach(App\service::get() as $servicio)
                            <tr>
                                <td>{{$servicio->nombre}}</td>
                                <td><small class="price">(${{$servicio->precio}})</small></td>
                                <td>
                                    <input type="hidden" value="{{$servicio->id}}" class="id-service">
                                    <input type="hidden" name="preprocess[]" class="preprocess">
                                    <input type="hidden" class="price" value="{{$servicio->precio}}">
                                    <img class="icon_menos" src="https://img.icons8.com/windows/25/000000/minus.png"> 
                                    <img class="icon_mas" src="https://img.icons8.com/windows/25/000000/add.png"> 
                                    @if($id_servicio == $servicio->id)
                                    <input style="width:50px;" type="number" class="input-content" value="1" size="2">
                                    @else
                                    <input readonly style="width:50px;" type="number" class="input-content" value="0" size="2">
                                    @endif
                                </td>
                                    @if($id_servicio == $servicio->id)
                                    <td class="subtotal">$<a class="price-sub">{{$servicio->precio}}</a></td>
                                    @else
                                    <td class="subtotal">$<a class="price-sub">0</a></td>
                                    @endif
                            </tr>
                        @endforeach
                        <tr>
                            <th colspan="2"></th>
                            <th>Total</th>
                            <th>
                                $<a class="total-price">0</a>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="2"></th>
                            <td colspan="2">
                                <button class="btn square_btn_grey btn-success btn-block ">Pagar ahora</button>
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>

    <script>
        //Calculadora

        $().ready(function(){
            //Precarga
            update_total();
            //restar
            $('.icon_menos').click(function(){
                $subtotal = $(this).siblings('.input-content').val();
                $(this).siblings('.input-content').val(parseInt($subtotal)-1);
                //Evita negativos
                if($(this).siblings('.input-content').val() < 0){
                    $(this).siblings('.input-content').val(0);
                }

                //Actualiza subtotal
                $sub_price = $(this).siblings('.input-content').val() * $(this).siblings('.price').val();

                $(this).parent().siblings('.subtotal').children('.price-sub').html($sub_price);
                update_total();

                //Para procesar 

                $id = $(this).siblings('.id-service').val();

                if(parseInt($subtotal)-1 < 0){
                    $subtotal = 0;
                }else{
                    $subtotal = parseInt($subtotal)-1;
                }

                $(this).siblings('.preprocess').val($id + ':' + $subtotal);
            });
            //sumar
            $('.icon_mas').click(function(){
                $subtotal = $(this).siblings('.input-content').val();
                $(this).siblings('.input-content').val(parseInt($subtotal)+1);
                //Evita negativos
                if($(this).siblings('.input-content').val() < 0){
                    $(this).siblings('.input-content').val(0);
                }

                //Actualiza subtotal
                $sub_price = $(this).siblings('.input-content').val() * $(this).siblings('.price').val();

                $(this).parent().siblings('.subtotal').children('.price-sub').html($sub_price);
                update_total();

                //Para procesar 

                $id = $(this).siblings('.id-service').val();
                if(parseInt($subtotal)+1 < 0){
                    $subtotal = 0;
                }else{
                    $subtotal = parseInt($subtotal)+1;
                }
                $(this).siblings('.preprocess').val($id + ':' + $subtotal);
            });
            
            //input form
            $('.input-content').on('change', function(){
                if($(this).val() < 0){
                    $(this).val(0);
                }
                // $sub_price = $(this).siblings('.input-content').val() * $(this).siblings('.price').val();

                // $(this).parent().siblings('.subtotal').children('.price-sub').html($sub_price);
                update_total();
            });
        });

        function update_total(){
            var sum = 0.0;
            $('.price-sub').each(function()
            {
                sum = parseFloat($(this).html()) + sum;
            });
            $('.total-price').html(sum);
        }
    </script>

@stop