@extends('index')

@section('container')

<?php
    //Se carga informacion del usuario si es edicion...
    if(isset($id)){
        $data = App\patient::find($id);
    }else{
        $data = null;
    }
    

    $mas = '';
    $fem = '';
    if(isset($data)){
        if($data->gender == 'Masculino'){
            $mas = 'Checked';
            $fem = '';
        }

        if($data->gender == 'Femenino'){
            $mas = '';
            $fem = 'Checked';
        }
    }
?>

<div class="container d-flex justify-content-center">
    <div class="col-12 col-lg-8" style="background-color:white; border-radius:15px;">
        
        <label class="section_name"><b>Registrar Un Familiar</b></label>

        <label style="color:grey; font-size:0.9em; margin-top:1em;">A continuación, llene todos los espacios para agregar un familiar a su cuenta.<br><b style="color:#226383;">NOTA: </b>Los campos marcados con <span style="color:red;">*</span> son obligatorios.</label>
            
        <form id="patient_form" style="margin-top:2em;" method="POST" action="{{route('guardar_paciente')}}">
            @csrf
            @isset($data)
                <input type="hidden" value="{{$id}}" name="edicion">
            @endif
            <div class="container">
                <label class="subtitle_label"><b>Información De Contacto</b></label>
                <div class="row">
                    <div class="form-group col-lg-4">
                        <label for="nombre_completo" class="input_label">Nombre completo <span style="color:red;">*</span></label>
                        <input type="text" value="<?php echo isset($data->name) ? $data->name: '' ;?>" class="custom_input form-control" id="nombre_completo" name="nombre_completo">
                        <div id="nombre_completo_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                    <div class="form-group col-lg-4 text-center">
                        <label for="sexo" class="input_label">Sexo <span style="color:red;">*</span></label>
                        <div class="form-check" style="margin-top:-0.5em;">
                            <input {{$mas}} class="sexo_radio form-check-input" type="radio" name="sexo" id="sexo_masculino" value="Masculino">
                            <label style="font-size:0.9em;color:grey;" class="form-check-label" for="sexo_masculino">Masculino</label>
                        </div>
                        <div class="form-check">
                            <input {{$fem}} class="sexo_radio form-check-input" type="radio" name="sexo" id="sexo_femenino" value="Femenino">
                            <label style="font-size:0.9em;color:grey;" class="form-check-label" for="sexo_femenino">Femenino</label>
                            <div class="invalid-feedback">
                                Por favor seleccione una opción
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="edad" class="input_label">Edad <span style="color:red;">*</span></label>
                        <input type="text" value="<?php echo isset($data->age) ? $data->age: '' ;?>" class="custom_input form-control" id="edad" name="edad">
                        <div id="edad_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-lg-4">
                        <label for="numero_telefono" class="input_label">Número de teléfono <span style="color:red;">*</span></label>
                        <input type="text" value="<?php echo isset($data->phone_number) ? $data->phone_number: '' ;?>" class="custom_input form-control" id="numero_telefono" name="numero_telefono">
                        <div id="numero_telefono_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="numero_celular" class="input_label">Número de teléfono móvil (celular)</label>
                        <input type="text" value="<?php echo isset($data->cellphone_phone) ? $data->cellphone_phone: '' ;?>" class="custom_input form-control" id="numero_celular" name="numero_celular">
                        <div id="numero_celular_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="consideraciones" class="input_label">Enfermedades y/o Consideraciones</label>
                        <input type="text" value="<?php echo isset($data->other) ? $data->other: '' ;?>" class="custom_input form-control" id="consideraciones" name="consideraciones">
                        <div id="consideraciones_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                </div>

                <label class="subtitle_label"><b>Información De Domicilio</b></label>
                <div class="row">
                    <div class="form-group col-lg-4">
                        <label for="calle" class="input_label">Calle <span style="color:red;">*</span></label>
                        <input type="text" value="<?php echo isset($data->street) ? $data->street: '' ;?>" class="custom_input form-control" id="calle" name="calle">
                        <div id="calle_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="numero_exterior" class="input_label">Número exterior <span style="color:red;">*</span></label>
                        <input type="text" value="<?php echo isset($data->ext_number) ? $data->ext_number: '' ;?>" class="custom_input form-control" id="numero_exterior" name="numero_exterior">
                        <div id="numero_exterior_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="colonia" class="input_label">Colonia <span style="color:red;">*</span></label>
                        <input type="text" value="<?php echo isset($data->neighborhood) ? $data->neighborhood: '' ;?>" class="custom_input form-control" id="colonia" name="colonia">
                        <div id="colonia_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                </div>

                <div class="col-12 text-center" style="margin-top:1em;">
                    <button type="button" id="submit_form_patient" class="custom_btn btn btn-info">Agregar Familiar</button>
                </div>
            </div>
        </form>

        <br><br>

    </div>
</div>

<script>
$(document).ready(function(){
    //Mask Inputs
    $('#numero_telefono').mask('(000) 000-0000 ext 0000');
    $('#numero_celular').mask('(000) 000-0000 ext 0000');
    $('#numero_exterior').mask('00000');

    //Submit Form
    $('#submit_form_patient').on('click',function(){
        var flag = 0;

        if($('#nombre_completo').val().trim() != ''){
            $('#nombre_completo').removeClass('is-invalid');
            flag ++;
        }else{
            $('#nombre_completo').addClass('is-invalid');
            flag = 0;
        }

        if($('.sexo_radio').is(':checked')){
            $('.sexo_radio').removeClass('is-invalid');
            flag ++;
        }else{
            $('.sexo_radio').addClass('is-invalid');
            flag = 0;
        }

        if($('#numero_telefono').val().trim() != ''){
            $('#numero_telefono').removeClass('is-invalid');
            flag ++;
        }else{
            $('#numero_telefono').addClass('is-invalid');
            flag = 0;
        }

        if($('#calle').val().trim() != ''){
            $('#calle').removeClass('is-invalid');
            flag ++;
        }else{
            $('#calle').addClass('is-invalid');
            flag = 0;
        }

        if($('#numero_exterior').val().trim() != ''){
            $('#numero_exterior').removeClass('is-invalid');
            flag ++;
        }else{
            $('#numero_exterior').addClass('is-invalid');
            flag = 0;
        }

        if($('#colonia').val().trim() != ''){
            $('#colonia').removeClass('is-invalid');
            flag ++;
        }else{
            $('#colonia').addClass('is-invalid');
            flag = 0;
        }   

        //Submit validation check
        if(flag == 6){
            $('#patient_form').submit();
        }
    })

    //Remove Class Invalid On KeyDown
    $('input[type=text]').on('keydown',function(){
        $(this).removeClass('is-invalid')
    })

    //Remove Class Invalid On checked
    $('.sexo_radio').on('change',function(){
        $('.sexo_radio').removeClass('is-invalid')
    })


});
</script>
@stop