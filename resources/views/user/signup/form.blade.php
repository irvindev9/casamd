@extends('index')
@section('container')

<div class="container">
    <div class="row d-flex justify-content-center">
        <form action="/user/signup/save" method="POST" class="col-12 col-md-8" style="border-radius:10px; background-color:#F0F1F8;">
            @csrf
            <div class="row d-flex justify-content-center" style="background-color:#349ED1; border-radius:10px 10px 0px 0px;">
                <img style="margin-top:1em; height:6em; width:auto;" src="{{URL::asset('../img/logo.svg')}}" class="card-img-top" alt="...">
                <div class="col-12 text-center" style="margin-bottom:0.3em;">
                    <h2 class="col-12" style="color:white;"><b>Registro</b></h2>
                </div>
            </div>

            <div class="row" style="margin-top:2em;">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="name" class="input_label">Nombre completo</label>
                        <input type="text" name="complete_name" class="custom_input custom_input form-control" id="name" placeholder="Ingrese su nombre completo">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="name" class="input_label">Numero celular</label>
                        <input type="text" name="cellphone_number" class="custom_input form-control" id="cellphone" placeholder="Ingrese su número de teléfono móvil">
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="name" class="input_label">Numero de teléfono fijo</label>
                        <input type="text" name="phone_number" class="custom_input form-control" id="phone_number" placeholder="Ingrese su número de teléfono fijo">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="name" class="input_label">Colonia/Fraccionamiento</label>
                        <input type="text" name="neighborhood" class="custom_input form-control" id="neighborhood" placeholder="Ingrese nombre de colonia/fraccionamiento">
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="name" class="input_label">Calle</label>
                        <input type="text" name="street" class="custom_input form-control" id="street" placeholder="Ingrese nombre de su calle">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="name" class="input_label">Numero exterior</label>
                        <input type="text" name="external_number" class="custom_input form-control" id="external_number" placeholder="Ingrese número exterior de su dirección">
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="name" class="input_label">Numero interior</label>
                        <input type="text" name="internal_number" class="custom_input form-control" id="internal_number" placeholder="Ingrese numero interior de su dirección">
                    </div>
                </div>
            </div>

            <div class="row" style="margin-bottom:2em;">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="name" class="input_label">Contraseña</label>
                        <input type="password" name="password" class="custom_input form-control" id="password" placeholder="Ingrese contraseña para la plataforma">
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="name" class="input_label">Correo electrónico</label>
                        <input type="email" name="email" class="custom_input form-control" id="email" placeholder="Ingrese su correo electrónico">
                    </div>
                </div>
            </div>

            <button type="submit" class="btn submit_btn btn-block btn-info">Registrarse</button>

        </form>
    </div>
</div>


<!-- Terms Modal -->

<!-- Button trigger modal -->
<button type="button" id="terms_modal" class="btn btn-primary d-none" data-toggle="modal" data-target="#Terms_moldal_div"></button>

<!-- Modal -->
<div class="modal fade" id="Terms_moldal_div" tabindex="-1" role="dialog" aria-labelledby="Terms_moldal_divLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    <div class="modal-header d-flex justify-content-center" style="background-color:#349ED1; color:white;">
        <h5 class="modal-title" id="Terms_moldal_divLabel">Términos y condiciones de servicio</h5>
    </div>
    <div class="modal-body" style="color:grey; height:20em; overflow-y: scroll;">
        <p style="text-align:justify;">
            Los presentes términos y condiciones generales son aplicables al uso de la plataforma tecnológica ofrecida por CasaMD sus marcas registradas, sus licenciatarios y/o filiales dentro de la App denominada CasaMD. El Usuario debe leer, entender y aceptar todas las condiciones establecidas en los Términos y Condiciones Generales y en el Aviso de Privacidad, previo en su inscripción como Usuario del CasaMD.
        </p>
        <p style="text-align:justify;">
            Los servicios ofrecidos por CasaMD solamente constituyen una plataforma de tecnología que permite a los usuarios de aplicaciones móviles, en este caso CasaMD, organizar y planear los servicios médicos a domicilio proporcionados por terceros independientes (profesionales de la salud). CasaMD no respalda ni es responsable por dichos servicios de terceros. Asimismo, CasaMD no será responsable de daños indirectos, incidentales, especiales, punitivos o emergentes, la perdida de datos, la lesión personal o el daño de la propiedad, ni de prejuicios relativos a los servicios proporcionados por terceros (profesionales de la salud).
        </p>
        <p style="text-align:justify;">
            La plataforma tecnológica mencionada es solo para su uso personal, no comercial. Usted reconoce que CasaMD no presta servicios médicos ni de atención médica por profesionales de la salud, si no que dichos servicios se prestan por terceros independientes que no estén empleados por CasaMD.
        </p>

        <p style="text-align:justify;">
            <b>1. CAPACIDAD. </b>Los servicios médicos proporcionados por terceros independientes a través de la plataforma tecnológica proporcionada por CasaMD (en lo sucesivo “los servicios”), solo están disponibles para personas que tengan capacidad legal para contratar y residir dentro de la República Mexicana. No podrán utilizar los servicios las personas que no tengan esa capacidad ni por menores de edad. 
        </p>
        <p style="text-align:justify;">
            <b>2. INSCRIPCION. </b>Usted podrá descargar la aplicación de CasaMD, y con el fin de acceder a “los servicios”, deberá proporcionar información personal para registrarse y crear una cuenta personal (“cuenta”). La recopilación y el uso de la información personal que proporcione es conforme a lo dispuesto en la Política Privacidad (“Aviso de Privacidad”), el cual lo puede usted consultar en nuestra página www.CasaMD.mx.  
        </p>
        <p style="text-align:justify;">
            <b>3. TITULARIDAD. </b>Todos los derechos de propiedad industrial relativos a estos son y seguirán siendo propiedad de CasaMD. Ninguna de sus Condiciones ni en el uso de los Servicios, no se podrá reducir, modificar, realizar obras derivadas sobre los Servicios o bien, a utilizar las marcas, logotipos y/o diseños. No se podrá retirar cualquier distintivo sobre derechos de autor y/o marca registrada respecto de la App y/o de los servicios, no se podrá reproducir, modificar, realizar obras derivadas sobre los Servicios y/o la App, distribuir, licenciar, arrendar, revender, transferir, exhibir públicamente, presentar públicamente, transmitir, retransmitir o explotar de otra forma los Servicios, excepto cuando se permita expresamente por CasaMD.
        </p>
        <p style="text-align:justify;">
            <b>4. ACCESO A LA RED. </b>El usuario es responsable de obtener el acceso a la red de datos necesarios para utilizar los Servicios. Se aplicaran las tarifas y tasas de datos de su proveedor de telefonía móvil y usted será responsable del pago de dichas tarifas y tasas, además, la funcionalidad de la plataforma tecnológica podrá ser objeto de disfunciones o retrasos inherentes al uso de internet y de las comunicaciones electrónicas.
        </p>
        <p style="text-align:justify;">
            <b>5. ATENCION MEDICA A DOMICILIO. </b>CasaMD es un proveedor que proporciona información y un médico (plataforma electrónica) para conectar a los Usuarios, con Profesionales de la Salud independientes, con cedula profesional para la práctica de la medicina en México (en lo sucesivo el Medico) ofreciendo servicios médicos (no de emergencia),a pacientes adultos y menores de edad en la comodidad de su casa, oficina o lugar donde se encuentre y donde desee que le brinden la atención medica señalada a través de su Aplicación móvil.
        </p>
    </div>
    <div class="modal-footer">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <a class="btn btn-danger" href="/" style="margin-right:1em; background-color:#ef5350; border:1px solid #ef5350;">Cancelar registro</a>
                <button type="button" class="btn normal_btn btn-info" data-dismiss="modal">Aceptar y continuar con el registro</button>
            </div>
        </div>
    </div>
    </div>
</div>
</div>

<script>
$(document).ready(function(){
    $('#terms_modal').click();
});

//Evita que se cierre modal al click fuera de el
$('#Terms_moldal_div').modal({backdrop: 'static', keyboard: false})
</script>

@stop