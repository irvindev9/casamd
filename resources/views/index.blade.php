<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Jquery JS -->
        <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.js') }}"></script>

        <script src="{{ asset('js/funciones_generales.js') }}"></script>
        <!-- Jquery Mask JS -->
        <script type="text/javascript" src="{{ asset('js/jquery.mask.js') }}"></script>
        <!-- Bootstrap css -->
        <link href="{{ asset('bootstrap/css/bootstrap.css') }}" rel="stylesheet">
        <!-- Estilos -->
        <link href="{{ asset('css/estilos.css') }}" rel="stylesheet">
        <!-- Bootstrao JS -->
        <script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.js') }}"></script>
        <!-- Popper JS CDN -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        
        <title>CASA MD</title>

    </head>
    <body>

        @include('header')

        <br><br>
        
        @yield('container')

        <br><br>

    </body>
</html>
