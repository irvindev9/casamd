@extends('index')

@section('container')
<div class="container" style="margin-top:2vh;">
    <div class="row d-flex justify-content-center">
        <div class="col-10 col-sm-8 col-md-6 col-lg-4" style="border-radius:15px; border:solid 1px #226383; background-color:#EFF3F5; ">
            <div class="row d-flex justify-content-center" style="background-color:#226383; padding:0px; border-radius:15px 15px 0px 0px;">
                <img style="margin-top:1em; margin-bottom:1em; height:6em; width:auto;" src="{{URL::asset('../img/logo.svg')}}" class="card-img-top" alt="...">
            </div>
            <form method="POST" action="/login" style="margin-top:2em; margin-bottom:2em;">
                @csrf
                <div class="form-group {{$errors->has('email') ? 'has-error' : '' }}">
                    <label class="input_label" for="user_email">Correo electrónico</label>
                    <input type="text" name="user_email" class="custom_input form-control" id="user_email" aria-describedby="emailHelp" value="{{old('user_email')}}">
                    {!! $errors->first('email','<span class="help-block">:message</span>') !!}
                </div>
                <div class="form-group {{$errors->has('password') ? 'has-error' : '' }}">
                    <label class="input_label" for="user_password">Contraseña</label>
                    <input type="password" name="user_password" class="custom_input form-control" id="user_password" >
                    {!! $errors->first('password','<span class="help-block">:message</span>') !!}
                </div>
                <button type="submit" class="btn btn-block custom_btn">Iniciar sesión</button>
            </form>
        </div>
    </div>
</div>
@stop