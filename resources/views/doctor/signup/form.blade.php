@extends('index')

@section('container')

<?php
    //Se carga informacion del usuario si es edicion...
    $data = array();
?>

<div class="container d-flex justify-content-center">
    <div class="col-12 col-lg-8" style="background-color:white; border-radius:15px;">
        
        <label class="section_name"><b>Registro de doctor</b></label>

        <label class="instructions_label">A continuación, llene todos los espacios para agregar un familiar a su cuenta.<br><b class="subtitle_label">NOTA: </b>Los campos marcados con <span style="color:red;">*</span> son obligatorios.</label>
            
        <form id="new_doctor_form" style="margin-top:2em;" method="POST" action="{{route('doctor_save')}}" enctype="multipart/form-data">
            @csrf
            <div class="container">

                <label class="subtitle_label"><b>Información general</b></label>

                <div class="row">
                    <div class="form-group col-lg-4">
                        <label for="nombre_completo" class="input_label">Nombre completo <span style="color:red;">*</span></label>
                        <input type="text" value="<?php echo isset($data->nombre_completo) ? $data->nombre_completo: '' ;?>" class="custom_input form-control" id="nombre_completo" name="nombre_completo">
                        <div id="nombre_completo_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="correo_electronico" class="input_label">Correo electrónico <span style="color:red;">*</span></label>
                        <input type="text" value="<?php echo isset($data->correo_electronico) ? $data->correo_electronico: '' ;?>" class="custom_input form-control" id="correo_electronico" name="correo_electronico">
                        <div id="correo_electronico_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="conrima_correo_electronico" class="input_label">Confirme correo electrónico <span style="color:red;">*</span></label>
                        <input type="text" value="<?php echo isset($data->correo_electronico) ? $data->correo_electronico: '' ;?>" class="custom_input form-control" id="conrima_correo_electronico" name="conrima_correo_electronico">
                        <div id="conrima_correo_electronico_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-lg-3">
                        <label for="telefono_principal" class="input_label">Telefono principal <span style="color:red;">*</span></label>
                        <input type="text" value="<?php echo isset($data->telefono_principal) ? $data->telefono_principal: '' ;?>" class="custom_input form-control" id="telefono_principal" name="telefono_principal">
                        <div id="telefono_principal_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="telefono_adicional" class="input_label">Telefono adicional</label>
                        <input type="text" value="<?php echo isset($data->telefono_adicional) ? $data->telefono_adicional: '' ;?>" class="custom_input form-control" id="telefono_adicional" name="telefono_adicional">
                        <div id="telefono_adicional_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="universidad_egreso" class="input_label">Universidad de egreso <span style="color:red;">*</span></label>
                        <input type="text" value="<?php echo isset($data->universidad_egreso) ? $data->universidad_egreso: '' ;?>" class="custom_input form-control" id="universidad_egreso" name="universidad_egreso">
                        <div id="universidad_egreso_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label for="edad" class="input_label">Edad <span style="color:red;">*</span></label>
                        <input type="text" value="<?php echo isset($data->edad) ? $data->edad: '' ;?>" class="custom_input form-control" id="edad" name="edad">
                        <div id="edad_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                </div>

                <div class="row d-flex justify-content-center">
                    <div class="form-group col-lg-4">
                        <label for="password" class="input_label">Defina la contraseña de acceso <span style="color:red;">*</span></label>
                        <input type="password" value="<?php echo isset($data->password) ? $data->password: '' ;?>" class="custom_input form-control" id="password" name="password">
                        <div id="password_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="conf_password" class="input_label">Confirme contraseña <span style="color:red;">*</span></label>
                        <input type="password" value="<?php echo isset($data->password) ? $data->password: '' ;?>" class="custom_input form-control" id="conf_password" name="conf_password">
                        <div id="conf_password_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                </div>

                <div class="row" style="margin-bottom:1em;">
                    <div class="col-12 text-center">
                        <label style="color:#33C2B7; font-size:0.9em;">Por favor defina una foto de perfil para su cuenta</label>
                    </div>
                    <div class="form-group col-12 text-center">
                        <img class="rounded-circle" id="profile_img" style="height:10em; width:10em;" src="https://www.uic.mx/posgrados/files/2018/05/default-user.png">
                    </div>
                    <div class="col-12 d-flex justify-content-center">
                        <div class="col-8">
                            <div class="custom-file">
                                <input type="file" name="file" class="custom-file-input" id="select_profile_img">
                                <label class="custom-file-label" for="select_profile_img" aria-describedby="inputGroupFileAddon02">Seleccionar archivo</label>
                            </div>
                        </div>
                    </div>
                </div>

                <!--label class="subtitle_label"><b>Información de domicilio</b></label><br>
                <label style="color:#33C2B7; font-size:0.9em;">La información de su domicilio no se mostrará a los usuarios</label>
                <div class="row">
                    <div class="form-group col-lg-4">
                        <label for="calle" class="input_label">Calle <span style="color:red;">*</span></label>
                        <input type="text" value="<?php /*echo isset($data->calle) ? $data->calle: '' ;*/?>" class="custom_input form-control" id="calle" name="calle">
                        <div id="calle_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="num_exterior" class="input_label">Numero exterior <span style="color:red;">*</span></label>
                        <input type="text" value="<?php /*echo isset($data->num_exterior) ? $data->num_exterior: '' ;*/?>" class="custom_input form-control" id="num_exterior" name="num_exterior">
                        <div id="num_exterior_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="num_interior" class="input_label">Numero interior</label>
                        <input type="text" value="<?php /*echo isset($data->num_interior) ? $data->num_interior: '' ;*/?>" class="custom_input form-control" id="num_interior" name="num_interior">
                        <div id="num_interior_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                </div> 
                <div class="row d-flex justify-content-center">
                    <div class="form-group col-lg-4">
                        <label for="colonia" class="input_label">Colonia <span style="color:red;">*</span></label>
                        <input type="text" value="<?php echo isset($data->colonia) ? $data->colonia: '' ;?>" class="custom_input form-control" id="colonia" name="colonia">
                        <div id="colonia_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="codigo_postal" class="input_label">Código postal <span style="color:red;">*</span></label>
                        <input type="text" value="<?php echo isset($data->codigo_postal) ? $data->codigo_postal: '' ;?>" class="custom_input form-control" id="codigo_postal" name="codigo_postal">
                        <div id="codigo_postal_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                </div-->

                <label class="subtitle_label"><b>Información de consultorio</b></label>
                <label style="color:#33C2B7; font-size:0.9em;">La ubicación de su consultorio si es publica y podrán verla los usuarios registrados en la plataforma</label>
                <div class="row">
                    <div class="form-group col-lg-4">
                        <label for="calle_consultorio" class="input_label">Calle</label>
                        <input type="text" value="<?php echo isset($data->calle_consultorio) ? $data->calle_consultorio: '' ;?>" class="custom_input form-control" id="calle_consultorio" name="calle_consultorio">
                        <div id="calle_consultorio_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="num_exterior_consultorio" class="input_label">Numero exterior</label>
                        <input type="text" value="<?php echo isset($data->num_exterior_consultorio) ? $data->num_exterior_consultorio: '' ;?>" class="custom_input form-control" id="num_exterior_consultorio" name="num_exterior_consultorio">
                        <div id="num_exterior_consultorio_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="num_interior_consultorio" class="input_label">Numero interior</label>
                        <input type="text" value="<?php echo isset($data->num_interior_consultorio) ? $data->num_interior_consultorio: '' ;?>" class="custom_input form-control" id="num_interior_consultorio" name="num_interior_consultorio">
                        <div id="num_interior_consultorio_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                </div>
                <div class="row d-flex justify-content-center">
                    <div class="form-group col-lg-4">
                        <label for="colonia_consultorio" class="input_label">Colonia</label>
                        <input type="text" value="<?php echo isset($data->colonia_consultorio) ? $data->colonia_consultorio: '' ;?>" class="custom_input form-control" id="colonia_consultorio" name="colonia_consultorio">
                        <div id="colonia_consultorio_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="codigo_postal_consultorio" class="input_label">Código postal</label>
                        <input type="text" value="<?php echo isset($data->codigo_postal_consultorio) ? $data->codigo_postal_consultorio: '' ;?>" class="custom_input form-control" id="codigo_postal_consultorio" name="codigo_postal_consultorio">
                        <div id="codigo_postal_consultorio_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                </div>
                <div class="row d-flex justify-content-center">
                    <div class="form-group col-lg-6">
                        <label for="codigo_postal" class="input_label">Detalles adicionales de consultorio (Atención general, especialidades, etc.)</label>
                        <textarea name="detalles_adicionales_consultorio" style="color:grey; font-size:0.8em;" class="form-control" id="detalles_adicionales_consultorio" rows="3"><?php echo isset($data->detalles_adicionales_consultorio) ? $data->detalles_adicionales_consultorio: '' ;?></textarea>
                        <div id="detalles_adicionales_consultorio_feed" class="invalid-feedback">Campo obligatorio</div>
                    </div>
                </div>

                <div class="col-12 text-center" style="margin-top:1em;">
                    <button type="submit" id="submit_form_new_doctor" class="custom_btn btn btn-info">Agregar doctor</button>
                </div>
            </div>
        </form>

        <br><br>

    </div>
</div>

<script>
$(document).ready(function(){
    //Mask Inputs
    $('#telefono_principal').mask('(000) 000-0000 ext 0000');
    $('#telefono_adicional').mask('(000) 000-0000 ext 0000');
    $('#cedula_profesional').mask('00000000');
    $('#num_exterior').mask('00000');
    $('#num_interior').mask('00000');
    $('#codigo_postal').mask('00000');
    $('#num_exterior_consultorio').mask('00000');
    $('#num_interior_consultorio').mask('00000');
    $('#codigo_postal_consultorio').mask('00000');
    

    //Submit Form
    $('#submit_form_new_doctor').on('click',function(){
        var flag = 0;

        if($('#nombre_completo').val().trim() != ''){
            flag++;
            $('#nombre_completo').removeClass('is-invalid');
        }else{
            flag = 0;
            $('#nombre_completo').addClass('is-invalid');
        }


        if($('#correo_electronico').val().trim() != '' && $('#conrima_correo_electronico').val().trim() != ''){
            if($('#correo_electronico').val() == $('#conrima_correo_electronico').val()){
                flag++;
                $('#correo_electronico').removeClass('is-invalid');
                $('#conrima_correo_electronico').removeClass('is-invalid');
            }else{
                flag = 0;
                $('#conrima_correo_electronico').addClass('is-invalid');
                $('#conrima_correo_electronico_feed').html('Los correos ingresados no coinciden, verifique e intente de nuevo');
            }
        }else{
            if($('#correo_electronico').val().trim() != ''){
                $('#correo_electronico').removeClass('is-invalid');
            }else{
                flag = 0;
                $('#correo_electronico').addClass('is-invalid');
            }

            if($('#conrima_correo_electronico').val().trim() != ''){
                $('#conrima_correo_electronico').removeClass('is-invalid');
            }else{
                flag = 0;
                $('#conrima_correo_electronico').addClass('is-invalid');
                $('#conrima_correo_electronico_feed').html('Campo obligatorio');
            }
        }

        if($('#telefono_principal').val().trim() != ''){
            flag++;
            $('#telefono_principal').removeClass('is-invalid');
        }else{
            flag = 0;
            $('#telefono_principal').addClass('is-invalid');
        }

        if($('#universidad_egreso').val().trim() != ''){
            flag++;
            $('#universidad_egreso').removeClass('is-invalid');
        }else{
            flag = 0;
            $('#universidad_egreso').addClass('is-invalid');
        }

        if($('#cedula_profesional').val().trim() != ''){
            flag++;
            $('#cedula_profesional').removeClass('is-invalid');
        }else{
            flag = 0;
            $('#cedula_profesional').addClass('is-invalid');
        }

        if($('#calle').val().trim() != ''){
            flag++;
            $('#calle').removeClass('is-invalid');
        }else{
            flag = 0;
            $('#calle').addClass('is-invalid');
        }

        if($('#num_exterior').val().trim() != ''){
            flag++;
            $('#num_exterior').removeClass('is-invalid');
        }else{
            flag = 0;
            $('#num_exterior').addClass('is-invalid');
        }

        if($('#colonia').val().trim() != ''){
            flag++;
            $('#colonia').removeClass('is-invalid');
        }else{
            flag = 0;
            $('#colonia').addClass('is-invalid');
        }

        if($('#codigo_postal').val().trim() != ''){
            flag++;
            $('#codigo_postal').removeClass('is-invalid');
        }else{
            flag = 0;
            $('#codigo_postal').addClass('is-invalid');
        }

        if($('#password').val().trim() != '' && $('#conf_password').val().trim() != ''){
            if($('#password').val() == $('#conf_password').val()){
                flag++;
                $('#password').removeClass('is-invalid');
                $('#conf_password').removeClass('is-invalid');
            }else{
                flag = 0;
                $('#conf_password').addClass('is-invalid');
                $('#conf_password_feed').html('Las contraseñas ingresadas no coinciden, verifique e intente de nuevo');
            }
        }else{
            if($('#password').val().trim() != ''){
                $('#password').removeClass('is-invalid');
            }else{
                flag = 0;
                $('#password').addClass('is-invalid');
            }

            if($('#conf_password').val().trim() != ''){
                $('#conf_password').removeClass('is-invalid');
            }else{
                flag = 0;
                $('#conf_password').addClass('is-invalid');
                $('#conf_password_feed').html('Campo obligatorio');
            }
        }

        if(flag == 10){
            $('#new_doctor_form').submit();
        }

    })

    //Profile Image
    $('#select_profile_img').on('change',function(){
        //Se borra preview de logo
        if(this.files && this.files[0]){
        //Nombre de archivo
        $('.custom-file-label').html(this.files[0].name);
        /* Asignamos el atributo source , haciendo uso del método createObjectURL*/
            $('#profile_img').attr('src', URL.createObjectURL(this.files[0]));
        }
    });

    //Remove Class Invalid On KeyDown
    $('input[type=text]').on('keydown',function(){
        $(this).removeClass('is-invalid')
    })

    //Remove Class Invalid On checked
    $('.sexo_radio').on('change',function(){
        $('.sexo_radio').removeClass('is-invalid')
    })


});
</script>
@stop