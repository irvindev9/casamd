<!DOCTYPE html>
<html>
<head>
    <title>Proveedor PDF</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    {{-- <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet" /> --}}

    <style>
        @page :first {
            /* background-image: url("http://pruebas.capim.com.mx/img/pdf/pdf-principal-logo.jpg") 0 0 no-repeat; */
            background-image-resize:1;
        }
        @page {
            /* background: url("http://pruebas.capim.com.mx/img/pdf/pdf-contenido.jpg") 0 0 no-repeat; */
            background-image-resize:1;
        }
        @page {
            margin-bottom:0px;
        }
        .page-break{
            page-break-after: always;
        }
    </style>
</head>
<body>

    <div class="row">

        <!-- Background Image -->
    <img style="z-index:-999; top:0px; left:0px; height:100%; width:115%; position:fixed; margin-top:-3em; margin-left:-3em;" src="./img/PDF/medical_certificate.jpg">

    <p style="margin-top:19em; margin-left:21em;">Irvin Raul Lopez Contreras</p>

    <p style="margin-top:-0.15em; margin-left:32em;">Masculino</p>

    <p style="margin-left:5em; margin-top:-0.25em;">18</p>

    <p style="margin-left:7em; margin-top:2.1em;">120/80</p>

    <p style="margin-left:23em; margin-top:-2.2em;">120</p>

    <p style="margin-left:38em; margin-top:-2.2em;">140</p>

    <p style="margin-left:8em; margin-top:-0.2em;">32.5</p>

    <p style="margin-left:22.5em; margin-top:-2.2em;">45 KG</p>

    <p style="margin-left:38.5em; margin-top:-2.2em;">34</p>

    <p style="margin-left:1em; line-height: 25pt; margin-top:3.2em;">Saludable, Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium quidem mollitia blanditiis obcaecati eos aut provident hic at quia excepturi voluptate nemo neque, facere distinctio, quasi accusamus vero eligendi et.</p>

    <p style="margin-left:38.5em; margin-top:6.4em;">Juarez Chih.</p>

    <p style="margin-left:35em; margin-top:-0.2em;">13</p>

    <p style="margin-left:17em; margin-top:-0.2em;">Julio</p>

    <p style="margin-left:38em; margin-top:-2.2em;">2019</p>

    <p style="margin-left:30em; margin-top:4.3em;">Alonso Garcia Del Rio</p>

    <p style="margin-left:34em; margin-top:-0.1em;">1596325874<br></p>

    <p style="margin-left:32em; margin-top:-0.3em; font-size:0.8em;">Universidad Autonoma De Cd. Juarez<br></p>
        
    </div>

</body>
</html>