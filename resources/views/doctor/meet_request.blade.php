@extends('index')

@section('container')

<?php
    //Se carga informacion del usuario si es edicion...
    $data = array();
?>

<div class="container">
    <div class="col-12" style="background-color:white; border-radius:15px;">

        <label class="section_name"><b>Solicitudes De Citas De Atención Medica</b></label>

        <br>

        <label class="instructions_label">A continuación se muestran las solicitudes de citas de atención medica que tiene actualmente</label>

        <div class="row" style="margin-top:1em;">
            <div class="container">
                <div class="card col-12" style="border-radius:15px; border:solid 1px #226383;">
                    <div class="card-body" style="padding:0px;">
                        <label style="color:black; height:2.5em; color:white; background-color:#226383; font-size:1.3em; padding-right:1em; padding-left:1em; margin-left:-0.7em; padding-top:0.5em; text-align:center; border-radius:15px 0px 15px 0px;"><b>Solicitante: </b>Irvin Raúl López Contreras</label>
                        <h5 class="card-title" style="color:grey;"><b style="color:#226383;">Razón De Cita: </b>Dolor Estomacal</h5>
                        <h6 class="card-subtitle" style="color:#226383; font-weight:bold;">Síntomas</h6>
                        <p class="card-text" style="color:grey;">Dolor de estómago, fiebre y nauseas..</p>

                        <h6 class="card-subtitle" style="color:#226383; font-weight:bold;">Información Adicional Del Usuario:</h6>

                        <p class="card-text" style="color:grey;"><b>Edad: </b>23 Años</p>
                        <p class="card-text" style="color:grey; margin-top:-1em;"><b>Dirección: </b>C. Nogal #3550 Col. Mezquital</p>
                        <p class="card-text" style="color:grey; margin-top:-1em;"><b>Numero celular: </b>(656) 352 0947</p>
                        <p class="card-text" style="color:grey; margin-top:-1em;"><b>Numero de teléfono fijo: </b>(656) 352 0947</p>
                        <p class="card-text" style="color:grey; margin-top:-1em;"><b>Correo electrónico: </b>irvin@gmail.com</p>

                        <div class="row" style="margin-bottom:2em;">
                            <div class="col-12 col-md-6">
                                <button type="button" style="background-color:#226383; border:solid 1px #226383;" class="btn btn-block btn-success">Aceptar Cita</button>
                            </div>
                            <div class="col-12 col-md-6">
                                <button type="button" class="btn btn-block btn-danger">Rechazar Cita</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br><br>

    </div>
</div>
@stop