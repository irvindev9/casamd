@extends('index')

@section('container')

<?php
    //Se carga informacion del usuario si es edicion...
    $data = array();
?>

<div class="container">
    <div class="col-12" style="background-color:#F0F1F8; border-radius:15px;">

        <label class="section_name"><b>Panel de control</b></label>

        <div class="row" style="margin-top:2em;">
            <div class="col-12 col-lg-4 align-self-center">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center">
                            <img class="rounded-circle" id="profile_img" style="height:10em; width:10em;" src="https://www.uic.mx/posgrados/files/2018/05/default-user.png">
                        </div>
                        <div class="col-12 text-center d-flex justify-content-center align-self-center" style="margin-top:1em;">
                            <h4><b style="color:#226383;">Dr. Alonso Garcia Del Rio</b></h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-8">
                <div class="container" style="border-radius:20px; background-color:white; border:solid 1px #CDCDCD;">
                    <div class="row">
                        <div class="container text-center" style="margin-top:1em;">
                            <h5 style="color:#226383;"><b>Recordatorio de citas</b></h5>
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row d-flex justify-content-center">

                            @if(isset(auth()->user()->id))
                            <?php

                            $citas_pendientes = DB::table('appointment_details')->where('appointment_date','>=',date("Y/m/d"))->where('doctor_id',auth()->user()->id)->where('is_acepted',1)->get();
                            ?>
                                @if(count($citas_pendientes) > 0)
                                    @foreach($citas_pendientes as $cita)

                                    <?php
                                    $user_patient = DB::table('users')->where('id',$cita->user_id)->first();

                                    //Array de meses
                                    $meses = array('01' => 'Enero','02' => 'Febrero','03' => 'Marzo','04' => 'Abril','05' => 'Mayo','06' => 'Junio','07' => 'Julio','08' => 'Agosto','09' => 'Septiembre','10' => 'Octubre','11' => 'Noviembre','12' => 'Diciembre');

                                    list($year, $month, $day) = explode("-", $cita->appointment_date);
                                    ?>
                                    <div class="card col-12 col-md-5 col-lg-3" style="border-radius:15px; border:solid 1.5px #349ED1; margin:1em;">
                                        <div class="card-body text-center" style="padding:5px">
                                            <h5 style="color:#226383; font-weight:bold; font-size:30px;">{{$day}}</h5>
                                            <h5 style="color:#226383; font-weight:bold; font-size:30px; margin-top:-0.4em;">{{$meses[$month]}}</h5>
                                            <h5 style="color:grey; font-weight:bold; font-size:16px; margin-top:-0.4em;">{{date("g:i a", strtotime($cita->appointment_time))}}</h5>
                                            <h5 style="color:grey; font-weight:bold; font-size:9px; margin-top:-0.4em;">{{$user_patient->username}}</h5>
                                        </div>
                                    </div>
                                    @endforeach
                                @else
                                <div class="card col-12 col-md-8" style="border-radius:15px; border:solid 1.5px #349ED1; margin:1em;">
                                    <div class="card-body text-center" style="padding:5px">
                                       <h3 style="color:#226383;">Lo sentimos. Por el momento no tienes citas próximas con pacientes.</h3>
                                    </div>
                                </div>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row" style="margin-top:2em;">
            
            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="card" style="border:solid 2px #CDCDCD; border-radius:10px; margin-top:1em;">
                    <div class="card-body text-center" style="padding:5px">
                        <h5 class="card-title" style="color:#226383; font-weight:bold;">Mis solicitudes de citas</h5>
                        <p class="card-text" style="color:#688191; letter-spacing:0.5px;">Use esta opción para ver si tiene nuevas solicitudes de citas y aceptar/rechazar las mismas.</p>
                        <a href="/doctor/meet/requests" class="btn custom_btn btn-info">Ver solicitudes</a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="card" style="border:solid 2px #CDCDCD; border-radius:10px; margin-top:1em;">
                    <div class="card-body text-center" style="padding:5px">
                        <h5 class="card-title" style="color:#226383; font-weight:bold;">Mis pacientes</h5>
                        <p class="card-text" style="color:#688191; letter-spacing:0.5px;">Use esta opción para ver la lista de los pacientes que han tenido cita con usted y detalles de la misma</p>
                        <a href="/doctor/patients_list" class="btn custom_btn btn-info">Ver pacientes</a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="card" style="border:solid 2px #CDCDCD; border-radius:10px; margin-top:1em;">
                    <div class="card-body text-center" style="padding:5px">
                        <h5 class="card-title" style="color:#226383; font-weight:bold;">Mis certificados médicos</h5>
                        <p class="card-text" style="color:#688191; letter-spacing:0.5px;">Use esta opción para ver todos los certificados médicos que ha realizado a través de CasaMD</p>
                        <a href="#" class="btn custom_btn btn-info">Ver certificados médicos</a>
                    </div>
                </div>
            </div>
        </div>

        <br><br>

    </div>
</div>
@stop