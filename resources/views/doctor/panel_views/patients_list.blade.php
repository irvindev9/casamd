@extends('index')

@section('container')

<?php
    $pacientes = DB::table('appointment_details')->where('doctor_id',auth()->user()->id)->where('is_acepted',1)->get();
?>


<div class="container">
    <div class="col-12" style="background-color:#F0F1F8; border-radius:15px;">

        <label class="section_name"><b>Mis Pacientes</b></label>

        <div class="row">
            <div class="col-12 text-right">
                <a href="/doctor/panel" class="btn custom_btn col-6 col-sm-4 col-md-3 col-lg-2 btn-info">Volver al panel</a> 
            </div>
        </div> 

        <br>

        <label class="instructions_label">A continuación se muestra el historial de pacientes que han interactuado con usted mediante CasaMD.</label>

        <div class="container" style="margin-top:2em;">

            <div class="row">

                @if(count($pacientes) > 0)
                    @foreach($pacientes as $paciente)

                        <?php
                        $user = DB::table('users')->where('id',$paciente->user_id)->first();

                        $service = DB::table('services')->where('id',(int) $paciente->type)->first();
                        
                        ?>

                        <div class="card col-12 col-lg-6">
                            <div class="row no-gutters">
                                <div class="col-md-4">
                                <img src="https://signup.trybooking.com/media/imgs/653488ddd926ab077de00f6d07ada3535e6dc186.png" class="card-img" alt="...">
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h5 class="card-title normal_label">{{$user->username}}</h5>
                                        <p class="card-text text_label"><b style="color:grey;">Servicio: </b> {{$service->nombre}}</p>
                                        <p class="card-text text_label"><b style="color:grey;">Detalles de cita: </b> {{$paciente->additional_info}}</p>
                                        <p class="card-text text_label"><b style="color:grey;">Fecha de cita: </b> {{$paciente->appointment_date}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
        <br><br>

    </div>
</div>
@stop