@extends('index')

@section('container')

<?php
    $solicitudes = DB::table('appointment_details')->where('doctor_id',auth()->user()->id)->where('is_acepted',0)->get();
?>

<div class="container">
    <div class="col-12" style="background-color:#F0F1F8; border-radius:15px;">

        <label class="section_name"><b>Solicitudes De Citas De Atención Medica</b></label>

        <div class="row">
            <div class="col-12 text-right">
                <a href="/doctor/panel" class="btn custom_btn col-12 col-lg-2 btn-info">Volver al panel</a> 
            </div>
        </div> 

        <br>

        <label class="instructions_label">A continuación se muestran las solicitudes de citas de atención medica que tiene actualmente</label>

        <div class="row" style="margin-top:1em;">
            <div class="container">
                @if(count($solicitudes))
                    @foreach($solicitudes as $solicitud)

                    <?php
                    $user = DB::table('users')->where('id',$solicitud->user_id)->first();

                    $user_info = DB::table('user_general_info')->where('user_id',$solicitud->user_id)->first();

                    $service = DB::table('services')->where('id',(int) $solicitud->type)->first();
                    
                    ?>
                    <div class="card col-12 col-md-6 col-lg-6" style="border-radius:15px; border:solid 1px #226383;">
                            <div class="card-body" style="padding:0px;">
                                <label style="color:black; height:2.5em; color:white; background-color:#226383; font-size:1em; padding-right:1em; padding-left:1em; margin-left:-0.95em; padding-top:0.5em; text-align:center; border-radius:15px 0px 15px 0px;"><b>Paciente: </b>{{$user->username}}</label><br>
                                <label style="color:#226383; font-size:0.9em;"><b>Tipo De Servicio:</b> <span style="color:grey; font-size:0.9em;">{{$service->nombre}}</span></label><br>
                                <label style="color:#226383; font-size:0.9em;"><b>Fecha:</b> <span style="color:grey; font-size:0.9em;">{{$solicitud->appointment_date}}</span></label><br>
                                <label style="color:#226383; font-size:0.9em;"><b>Hora:</b> <span style="color:grey; font-size:0.9em;">{{$solicitud->appointment_time}}</span></label><br>
                                <label style="color:#226383; font-size:0.9em;"><b>Información Adicional:</b> <span style="color:grey; font-size:0.9em;">{{$solicitud->additional_info}}</span></label><br>
                                @if($solicitud->place == 'Domicilio')
                                <label style="color:#226383; font-size:0.9em;"><b>NOTA:</b> <span style="color:grey; font-size:0.9em;">El paciente requiere servicio a domicilio</span></label><br>
                                @endif
                                <div class="container" style="margin-bottom:2em; margin-top:1em;">
                                    <div class="row text-center">
                                        <div class="col-12">
                                        <form action="/meet/status/update" method="POST">
                                            @csrf
                                            <input type="hidden" name="solicitud_id" class="custom_input custom_input form-control" value="{{$solicitud->id}}">
                                            <button type="button" data-user_phone="{{$user_info->cellphone_phone}}" data-appointment_id="{{$solicitud->id}}" class="btn btn_aceptar custom_btn btn-block btn-info">Aceptar y notificar</button>   
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else

                <h3 style="color:#226383;">Por el momento no tiene nuevas solicitudes de cita.</h3>

                @endif
            </div>
        </div>

        <br><br>

    </div>
</div>

<button type="button" class="btn_modal d-none btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Launch demo modal
</button>
    
<!-- Modal -->
<div class="modal fade" data-backdrop="static" data-keyboard="false" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="border-radius:15px; border:solid 2px #226383;">
        <div class="modal-header" style="padding:0px;">
            <label style="color:black; height:2.5em; color:white; background-color:#226383; font-size:1em; padding-right:1em; padding-left:1em; padding-top:0.5em; text-align:center; border-radius:13px 0px 15px 0px;"><b>Cita registrada</label><br>
        </div>
        <div class="modal-body" style="color:grey; font-size:0.9em;">
            Su cita se ha registrado correctamente.
            
            <br>Por favor póngase en contacto con el usuario a su número telefónico <span class="user_cellphone" style="color:#226383;">656 352 0947</span> para una mejor comunicación y confirmar la información de fecha y hora de la cita.
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary close_modal" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
</div>

<script>

$('.close_modal').on('click',function(){
    location.reload();
});

$(document).on("click",".btn_aceptar",function() {

    $('.user_cellphone').html($(this).attr('data-user_phone'));

    var parametros = {
        "solicitud_id" : $(this).attr('data-appointment_id'),
    };

    $.ajax({
        data:  parametros,
        url:   '/meet/status/update',
        type:  'GET',

        success:  function (result) {
            $('.btn_modal').click();
            
        },
        error: function(result){
            console.log(result);
        }
    });
});

</script>
@stop