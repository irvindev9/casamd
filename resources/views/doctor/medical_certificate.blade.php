@extends('index')

@section('container')

<?php
    //Se carga informacion del usuario si es edicion...
    $data = array();
?>

<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-12 col-sm-8" style="border-radius:15px; border:solid 1px white; background-color:white;">

            <br>

            <h2 class="text-center" style="color:#33C2B7;"><b>Certificado Medico</b></h2>

            <br>

            <form id="form_certificate">
                <div class="container">
                    <label style="color:#33C2B7;"><b>Datos generales del paciente</b></label>
                    <div class="row">
                        <div class="form-group col-lg-4">
                            <label for="nombre_completo" style="font-size:0.8em; color:grey;">Nombre completo</label>
                            <input type="text" value="<?php echo isset($data->nombre_completo) ? $data->nombre_completo: '' ;?>" style="height:2.2em; color:grey; font-size:0.8em;" class="form-control" id="nombre_completo" name="nombre_completo">
                            <div id="nombre_completo_feed" class="invalid-feedback">Campo obligatorio</div>
                        </div>
                        <div class="form-group col-lg-4 text-center">
                            <label for="sexo_paciente" style="font-size:0.8em; color:grey;">Sexo</label>
                            <div class="form-check" style="margin-top:-0.5em;">
                                <input class="sexo_radio form-check-input" type="radio" name="sexo_paciente" id="sexo_masculino" value="Masculino">
                                <label style="font-size:0.9em;color:grey;" class="form-check-label" for="sexo_masculino">Masculino</label>
                            </div>
                            <div class="form-check">
                                <input class="sexo_radio form-check-input" type="radio" name="sexo_paciente" id="sexo_femenino" value="Femenino">
                                <label style="font-size:0.9em;color:grey;" class="form-check-label" for="sexo_femenino">Femenino</label>
                                <div class="invalid-feedback">
                                    Por favor seleccione una opción
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-2">
                            <label for="edad_paciente" style="font-size:0.8em; color:grey;">Edad</label>
                            <input type="text" value="<?php echo isset($data->edad_paciente) ? $data->edad_paciente: '' ;?>" style="height:2.2em; color:grey; font-size:0.8em;" class="form-control" id="edad_paciente" name="edad_paciente">
                            <div id="edad_paciente_feed" class="invalid-feedback">Campo obligatorio</div>
                        </div>
                    </div>

                    <label style="color:#33C2B7;"><b>Datos médicos del paciente</b></label>
                    <div class="row">
                        <div class="form-group col-lg-3">
                            <label for="tension_arterial_paciente" style="font-size:0.8em; color:grey;">Tensión arterial</label>
                            <input type="text" value="<?php echo isset($data->tension_arterial_paciente) ? $data->tension_arterial_paciente: '' ;?>" style="height:2.2em; color:grey; font-size:0.8em;" class="form-control" id="tension_arterial_paciente" name="tension_arterial_paciente">
                            <div id="tension_arterial_paciente_feed" class="invalid-feedback">Campo obligatorio</div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="frecuencia_cardiaca_paciente" style="font-size:0.8em; color:grey;">Frecuencia cardiaca</label>
                            <input type="text" value="<?php echo isset($data->frecuencia_cardiaca_paciente) ? $data->frecuencia_cardiaca_paciente: '' ;?>" style="height:2.2em; color:grey; font-size:0.8em;" class="form-control" id="frecuencia_cardiaca_paciente" name="frecuencia_cardiaca_paciente">
                            <div id="frecuencia_cardiaca_paciente_feed" class="invalid-feedback">Campo obligatorio</div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="frecuencia_repiratoria_paciente" style="font-size:0.8em; color:grey;">Frecuencia respiratoria</label>
                            <input type="text" value="<?php echo isset($data->frecuencia_repiratoria_paciente) ? $data->frecuencia_repiratoria_paciente: '' ;?>" style="height:2.2em; color:grey; font-size:0.8em;" class="form-control" id="frecuencia_repiratoria_paciente" name="frecuencia_repiratoria_paciente">
                            <div id="frecuencia_repiratoria_paciente_feed" class="invalid-feedback">Campo obligatorio</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-3">
                            <label for="temperatura_paciente" style="font-size:0.8em; color:grey;">Temperatura</label>
                            <input type="text" value="<?php echo isset($data->temperatura_paciente) ? $data->temperatura_paciente: '' ;?>" style="height:2.2em; color:grey; font-size:0.8em;" class="form-control" id="temperatura_paciente" name="temperatura_paciente">
                            <div id="temperatura_paciente_feed" class="invalid-feedback">Campo obligatorio</div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="peso_paciente" style="font-size:0.8em; color:grey;">Peso</label>
                            <input type="text" value="<?php echo isset($data->peso_paciente) ? $data->peso_paciente: '' ;?>" style="height:2.2em; color:grey; font-size:0.8em;" class="form-control" id="peso_paciente" name="peso_paciente">
                            <div id="peso_paciente_feed" class="invalid-feedback">Campo obligatorio</div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="talla_paciente" style="font-size:0.8em; color:grey;">Talla</label>
                            <input type="text" value="<?php echo isset($data->talla_paciente) ? $data->talla_paciente: '' ;?>" style="height:2.2em; color:grey; font-size:0.8em;" class="form-control" id="talla_paciente" name="talla_paciente">
                            <div id="talla_paciente_feed" class="invalid-feedback">Campo obligatorio</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label for="estado_paciente" style="font-size:0.8em; color:grey;">Descripción de estado del paciente</label>
                            <textarea style="color:grey; font-size:0.8em;" name="estado_paciente" class="form-control" id="estado_paciente">
                            <?php echo isset($data->estado_paciente) ? $data->estado_paciente: '' ;?>
                            </textarea>
                            <div class="estado_paciente_invalid-feedback"></div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="ciudad_destino" style="font-size:0.8em; color:grey;">Ciudad</label>
                            <input type="text" value="<?php echo isset($data->ciudad_destino) ? $data->ciudad_destino: '' ;?>" style="height:2.2em; color:grey; font-size:0.8em;" class="form-control" id="ciudad_destino" name="ciudad_destino">
                            <div id="ciudad_destino_feed" class="invalid-feedback">Campo obligatorio</div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="fecha_elaboracion" style="font-size:0.8em; color:grey;">Fecha de elaboración</label>
                            <input type="text" value="<?php echo isset($data->fecha_elaboracion) ? $data->fecha_elaboracion: '' ;?>" style="height:2.2em; color:grey; font-size:0.8em;" class="form-control" id="fecha_elaboracion" name="fecha_elaboracion">
                            <div id="fecha_elaboracion_feed" class="invalid-feedback">Campo obligatorio</div>
                        </div>
                    </div>
                </div>

                <br>

                <div class="container">
                    <div class="text-center">
                        <button style="background-color:#33C2B7; border-color:#33C2B7;" id="submit_certificate" type="button" class="btn btn-primary">Guardar Certificado</button>
                    </div>
                </div>

                <br>

            </form>
        </div>
    </div>
</div>

<script>

    //Mask Inputs
    $('#edad_paciente').mask('000');
    $('#fecha_elaboracion').mask('00/00/0000');

    //Submit Form
    $('#submit_certificate').on('click',function(){
        var flag = 0;

        if($('#nombre_completo').val().trim() != ''){
            $('#nombre_completo').removeClass('is-invalid');
            flag ++;
        }else{
            $('#nombre_completo').addClass('is-invalid');
            flag = 0;
        }

        if($('.sexo_radio').is(':checked')){
            $('.sexo_radio').removeClass('is-invalid');
            flag ++;
        }else{
            $('.sexo_radio').addClass('is-invalid');
            flag = 0;
        }

        if($('#edad_paciente').val().trim() != ''){
            $('#edad_paciente').removeClass('is-invalid');
            flag ++;
        }else{
            $('#edad_paciente').addClass('is-invalid');
            flag = 0;
        }

        if($('#tension_arterial_paciente').val().trim() != ''){
            $('#tension_arterial_paciente').removeClass('is-invalid');
            flag ++;
        }else{
            $('#tension_arterial_paciente').addClass('is-invalid');
            flag = 0;
        }

        if($('#frecuencia_cardiaca_paciente').val().trim() != ''){
            $('#frecuencia_cardiaca_paciente').removeClass('is-invalid');
            flag ++;
        }else{
            $('#frecuencia_cardiaca_paciente').addClass('is-invalid');
            flag = 0;
        }

        if($('#frecuencia_repiratoria_paciente').val().trim() != ''){
            $('#frecuencia_repiratoria_paciente').removeClass('is-invalid');
            flag ++;
        }else{
            $('#frecuencia_repiratoria_paciente').addClass('is-invalid');
            flag = 0;
        }

        if($('#temperatura_paciente').val().trim() != ''){
            $('#temperatura_paciente').removeClass('is-invalid');
            flag ++;
        }else{
            $('#temperatura_paciente').addClass('is-invalid');
            flag = 0;
        }

        if($('#peso_paciente').val().trim() != ''){
            $('#peso_paciente').removeClass('is-invalid');
            flag ++;
        }else{
            $('#peso_paciente').addClass('is-invalid');
            flag = 0;
        }

        if($('#talla_paciente').val().trim() != ''){
            $('#talla_paciente').removeClass('is-invalid');
            flag ++;
        }else{
            $('#talla_paciente').addClass('is-invalid');
            flag = 0;
        }

        if($('#estado_paciente').val().trim() != ''){
            $('#estado_paciente').removeClass('is-invalid');
            flag ++;
        }else{
            $('#estado_paciente').addClass('is-invalid');
            flag = 0;
        }

        if($('#ciudad_destino').val().trim() != ''){
            $('#ciudad_destino').removeClass('is-invalid');
            flag ++;
        }else{
            $('#ciudad_destino').addClass('is-invalid');
            flag = 0;
        }

        if($('#fecha_elaboracion').val().trim() != ''){
            $('#fecha_elaboracion').removeClass('is-invalid');
            flag ++;
        }else{
            $('#fecha_elaboracion').addClass('is-invalid');
            flag = 0;
        }
        

        //Submit validation check
        if(flag == 12){
            $('#form_certificate').submit();
        }
    })

</script>
@stop