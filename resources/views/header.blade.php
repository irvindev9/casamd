<nav class="navbar navbar-expand-md navbar-light " style="background-color:#226383;box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.75);">
    <a class="navbar-brand" href="/"><span><img style="height:2em; width:auto;" src="{{URL::asset('../img/logo.svg')}}" class="card-img-top" alt="..."></span> <b style="color:white;">CasaMD</b></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto" style="color:white!important;">
        @isset(auth()->user()->username)
        <li class="nav-item active">
            <a class="nav-link" href="/user/home" style="color:white;">@isset(auth()->user()->username) {{auth()->user()->username}} @else User @endif</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/user/home" style="color:white;">Panel de usuario</a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:white;">
                Opciones
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Editar información de perfil</a>
            <a class="dropdown-item" href="/user/home/configuracion">Mi suscripcion</a>
            <a class="dropdown-item" href="#">Mis citas</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/logout">Cerrar sesión</a>
            </div>

        </li>
        @else
        <li class="nav-item">
            <a class="nav-link" href="/user/signup" style="color:white;">Regístrate</a>
        </li>
        @endif
        
        </ul>
        {{-- <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
        </form> --}}
    </div>
</nav>